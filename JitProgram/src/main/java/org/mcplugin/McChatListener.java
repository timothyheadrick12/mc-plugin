package org.mcplugin;

import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.ArrayList;

public class McChatListener implements Listener{

    private final String JOIN_SERVER_MSG_PATH = "discord.player-join-msg";
    private final String LEAVE_SERVER_MSG_PATH = "discord.player-leave-msg";

    private JitPlugin plugin;
    private String joinedServerMsg;
    private String leftServerMsg;

    public McChatListener(JitPlugin instance)//this needs to be in every class that needs to access the main plugin file
    {
        plugin = instance;
        joinedServerMsg = instance.getConfig().getString(JOIN_SERVER_MSG_PATH);
        leftServerMsg = instance.getConfig().getString(LEAVE_SERVER_MSG_PATH);
    }

    //descr: sends message to discord whenever registered used sends message in mc chat
    //pre: discordChatHandler must be initialized
    //post: message send in discord chat
    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event)//The new one sucks dick, so Im using deprecated
    {
        Player player = event.getPlayer();
        String playerMsg = event.getMessage();
        if(PlayerDataHandler.isRegistered(player)){
            DiscordChatHandler.sendMessagedInMain(PlayerDataHandler.getTagfromPlayer(player), playerMsg);
            System.out.println("Attempted to send message in discord!");
        }
    }

    @EventHandler
    public void playerDeath(PlayerDeathEvent event){
        DiscordChatHandler.sendServerMessageInMain(event.getDeathMessage());
    }

    @EventHandler
    public void playerJoin(PlayerJoinEvent event){
        String name = event.getPlayer().getDisplayName();
        DiscordChatHandler.sendServerMessageInMain(name + joinedServerMsg);
        DiscordChatHandler.updateOnlinePlayersList();

    }

    @EventHandler
    public void playerQuit(PlayerQuitEvent event){
        String name = event.getPlayer().getDisplayName();
        DiscordChatHandler.sendServerMessageInMain(name + leftServerMsg);
        DiscordChatHandler.updateOnlinePlayersList();
        BukkitScheduler scheduler = plugin.getServer().getScheduler();
        scheduler.scheduleSyncDelayedTask(plugin, new Runnable() {//this makes sure the player is gone before updating playerList
            @Override
            public void run() {
                DiscordChatHandler.updateOnlinePlayersList();
            }
        }, 10L);
    }
}

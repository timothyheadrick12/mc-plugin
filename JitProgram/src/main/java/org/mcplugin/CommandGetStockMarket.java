package org.mcplugin;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Map;

public class CommandGetStockMarket implements CommandExecutor {

    private JitPlugin plugin;
    private StockMarketListener stockMarket;

    CommandGetStockMarket(JitPlugin instance){
        plugin = instance;
        stockMarket = instance.getStockMarket();
    }

    @Override//called when command is run
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(sender instanceof Player){
            Player player = (Player) sender;
            player.sendMessage(stockMarketMessageInGame());
            return true;
        }
        return false;
    }

    private String stockMarketMessageInGame(){
        Map<Material, Double> stockMarketChange = plugin.getStockMarket().comparePresentMarketToOld();
        Map<Material, Integer> stockMarket = plugin.getStockMarket().getItemStockMarket();
        StringBuilder stockMarketMessageString = new StringBuilder(1000);
        stockMarketMessageString.append("Current Item Tracking Compared to last Save\n")
                .append("-------------------------------------------\n");
        for(Material item : stockMarket.keySet()){
            stockMarketMessageString.append(item.toString());
            stockMarketMessageString.append(" : ").append(stockMarket.get(item).toString())
                    .append(" : ").append(stockMarketChange.get(item).intValue()).append("% change since last save")
                    .append("\n");

        }
        stockMarketMessageString.append("-------------------------------------------");
        return stockMarketMessageString.toString();
    }
}

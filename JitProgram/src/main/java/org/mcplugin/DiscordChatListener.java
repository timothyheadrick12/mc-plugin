package org.mcplugin;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.bukkit.Bukkit;

public class DiscordChatListener extends ListenerAdapter {


    //descr: Sends message to minecraft server whenever registered user sends message in MC CHAT channel
    //pre: bot must be initialized
    //post: message sent to minecraft chat
    @Override
    public void onMessageReceived(MessageReceivedEvent event){
        User author = event.getAuthor();
        Message message = event.getMessage();
        MessageChannel channel = event.getChannel();
        boolean authorIsRegistered = PlayerDataHandler.isRegistered(author.getAsTag());
        if(channel.getIdLong() == DiscordChatHandler.mainTextChannel.getIdLong() && authorIsRegistered){
            String msg = message.getContentDisplay();//this is a readable version of message
            Bukkit.broadcastMessage("<" + DiscordChatHandler.nameWithoutTag(author.getAsTag()) + "> "
                    + msg);
        }
    }

}

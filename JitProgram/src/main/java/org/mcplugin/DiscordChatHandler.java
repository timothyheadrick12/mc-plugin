package org.mcplugin;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.exceptions.ErrorResponseException;
import net.dv8tion.jda.api.requests.ErrorResponse;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Map;

public class DiscordChatHandler {
    public static final String CONFIG_GUILD_PATH = "discord.server-id";
    public static final String REGISTERED_ROLE_PATH = "discord.registered-role-id";
    public static final String REGISTERED_ROLE_NAME = "registered";
    public static final String MAIN_CATEGORY_PATH = "discord.category-id";
    public static final String MAIN_CATEGORY_NAME = "MINECRAFT";
    public static final String MAIN_TEXT_CHANNEL_PATH = "discord.main-chat-id";
    public static final String MAIN_TEXT_CHANNEL_NAME = "mc-chat";
    public static final String STOCK_TEXT_CHANNEL_PATH = "discord.stock-market-id";
    public static final String STOCK_TEXT_CHANNEL_NAME = "mc-stock-market";
    public static final String STOCK_TEXT_CHANNEL_MSG_PATH = "discord.stock-market-id-msg";
    public static final String ONLINE_PLAYERS_TEXT_CHANNEL_PATH = "discord.online-players-id";
    public static final String ONLINE_PLAYERS_TEXT_CHANNEL_NAME = "online-players";
    public static final String ONLINE_PLAYERS_MSG_PATH = "discord.online-players-id-msg";

    public static boolean initialized = false;
    public static JitPlugin plugin;
    public static JDA bot;
    public static Guild guild;
    public static Category mainCategory;
    public static TextChannel mainTextChannel;
    public static TextChannel stockMarketTextChannel;
    public static Message stockMarketMessage;
    public static TextChannel onlinePlayersChannel;
    public static Message onlinePlayersMessage;
    public static Role registeredRole;


    //descr: Initializes the handler. The handler will not work before this is done
    //       every queue creates what is searched for when it is missing
    //pre: must pass main plugin
    //post: DiscordChatHandler initialized and ready to run
    public static void initializeHandler(JitPlugin passedPlugin)
    {
        plugin = passedPlugin;
        bot = plugin.getBot();
        guild = bot.getGuildById(plugin.getConfig().getLong(CONFIG_GUILD_PATH));//this is the server the bot resides in
        System.out.println("Succesfully connected to guild: " + guild);
        if(guild.getRoleById(plugin.getConfig().getLong(REGISTERED_ROLE_PATH)) == null)
        {
            guild.createRole().setName(REGISTERED_ROLE_NAME)
                    .queue(role -> {
                        plugin.getConfig().set(REGISTERED_ROLE_PATH, role.getIdLong());
                        plugin.saveConfig();
                        plugin.reloadConfig();
                        registeredRole = guild.getRoleById(role.getIdLong());
                        System.out.println("Successfully created role: " + role);
                        categoryAndChannelMaker();
                    });
        }
        else{
            registeredRole = guild.getRoleById(plugin.getConfig().getLong(REGISTERED_ROLE_PATH));
            System.out.println("Successfully got role: " + registeredRole);
            categoryAndChannelMaker();
        }

        initialized = true;
    }

    //makes main categroy and main text channel/stockTextChannel or just main text channel
    // and/or stockMarketTextChannel if category exists but one or both don't.
    public static void categoryAndChannelMaker()
    {
        if(guild.getCategoryById(plugin.getConfig().getLong(MAIN_CATEGORY_PATH)) == null) {//if category doesn't exist it creates it and the main channel that should be within
            guild.createCategory(MAIN_CATEGORY_NAME)
                    .queue(category -> {
                        plugin.getConfig().set(MAIN_CATEGORY_PATH, category.getIdLong());
                        mainCategory = category;//mainCategory set
                        System.out.println("Successfully created category: " + category);
                        mainChannelMaker();
                        stockMarketChannelMaker();
                        onlinePlayersChannelMaker();
                    });
        }
        else {
            mainCategory = guild.getCategoryById(plugin.getConfig().getLong(MAIN_CATEGORY_PATH));
            System.out.println("Succesfully connected to category: " + mainCategory);
            if (guild.getTextChannelById(plugin.getConfig().getLong(MAIN_TEXT_CHANNEL_PATH)) == null) {//if category exists but channel does not
                mainChannelMaker();
            }
            else {
                mainTextChannel = guild.getTextChannelById(plugin.getConfig().getLong(MAIN_TEXT_CHANNEL_PATH));
                System.out.println("Succesfully connected to channel: " + mainTextChannel);
            }
            if (guild.getTextChannelById(plugin.getConfig().getLong(STOCK_TEXT_CHANNEL_PATH)) == null) {//if category exists but channel does not
                stockMarketChannelMaker();
            }
            else {
                stockMarketTextChannel = guild.getTextChannelById(plugin.getConfig().getLong(STOCK_TEXT_CHANNEL_PATH));
            }
            if (guild.getTextChannelById(plugin.getConfig().getLong(ONLINE_PLAYERS_TEXT_CHANNEL_PATH)) == null) {//if category exists but channel does not
                onlinePlayersChannelMaker();
            }
            else {
                onlinePlayersChannel = guild.getTextChannelById(plugin.getConfig().getLong(ONLINE_PLAYERS_TEXT_CHANNEL_PATH));
            }
        }
    }

    //makes main text channel
    public static void mainChannelMaker(){
        mainCategory.createTextChannel(MAIN_TEXT_CHANNEL_NAME)
                .addRolePermissionOverride(registeredRole.getIdLong(), EnumSet.of(Permission.MESSAGE_WRITE), null)
                .addRolePermissionOverride(guild.getIdLong(), null, EnumSet.of(Permission.MESSAGE_WRITE))
                .queue(channel -> {
                    plugin.getConfig().set(MAIN_TEXT_CHANNEL_PATH, channel.getIdLong());
                    plugin.saveConfig();
                    plugin.reloadConfig();//need to reload config to set from config
                    mainTextChannel = channel;
                    System.out.println("Successfully created channel: " + channel);
                });
    }

    public static void stockMarketChannelMaker(){
        mainCategory.createTextChannel(STOCK_TEXT_CHANNEL_NAME)
                .addRolePermissionOverride(guild.getIdLong(), null, EnumSet.of(Permission.MESSAGE_WRITE))
                .queue(channel -> {
                    plugin.getConfig().set(STOCK_TEXT_CHANNEL_PATH, channel.getIdLong());
                    plugin.saveConfig();
                    plugin.reloadConfig();//need to reload config to set from config
                    stockMarketTextChannel = channel;
                    createStockMarketMessage();
                    System.out.println("Successfully created channel: " + channel);
                });
    }

    public static void createStockMarketMessage(){
        String message = stockMarketMessageFormat();
        stockMarketTextChannel.sendMessage(message).queue(msg -> {//gets message id after sending
            plugin.getConfig().set(STOCK_TEXT_CHANNEL_MSG_PATH, msg.getIdLong());
            plugin.saveConfig();
            plugin.reloadConfig();//need to reload config to set from config
            stockMarketMessage = msg;
        });
    }

    public static void onlinePlayersChannelMaker(){
        mainCategory.createTextChannel(ONLINE_PLAYERS_TEXT_CHANNEL_NAME)
                .addRolePermissionOverride(guild.getIdLong(), null, EnumSet.of(Permission.MESSAGE_WRITE))
                .queue(channel -> {
                    plugin.getConfig().set(ONLINE_PLAYERS_TEXT_CHANNEL_PATH, channel.getIdLong());
                    plugin.saveConfig();
                    plugin.reloadConfig();//need to reload config to set from config
                    onlinePlayersChannel = channel;
                    createOnlinePlayersMessage();
                    System.out.println("Successfully created channel: " + channel);
                });
    }

    public static void createOnlinePlayersMessage(){
        String message = onlinePlayersListFormat();
        onlinePlayersChannel.sendMessage(message).queue(msg -> {//gets message id after sending
            plugin.getConfig().set(ONLINE_PLAYERS_MSG_PATH, msg.getIdLong());
            plugin.saveConfig();
            plugin.reloadConfig();//need to reload config to set from config
            onlinePlayersMessage = msg;
        });
    }


    //desc: validates tag to make sure it gets a member
    public static boolean validTag(String tag){
        try {
            return guild.getMemberByTag(tag) != null;
        } catch(IllegalArgumentException e){
            e.printStackTrace();
            return false;
        }
    }
    //gets member from a tag
    //pre: tag must be validated
    public static Member getMemberFromTag(String tag){
        return guild.getMemberByTag(tag);
    }

    //descr: gives member the registered role
    //pre: tag must be validated
    //post: member registered
    public static void register(String tag){
        Member member = getMemberFromTag(tag);
        guild.addRoleToMember(member, registeredRole).queue();//genuinely no fucking clue why but queue is required here.
        System.out.println("Registered: " + tag);
    }

    public static String nameWithoutTag(String tag){
        return tag.substring(0, tag.length() - 5);
    }

    public static void sendMessagedInMain(String tag, String message){
        mainTextChannel.sendMessage("<" + nameWithoutTag(tag) + "> " + message).queue();
        System.out.println("Sent message in channel " + mainTextChannel);
    }

    public static void sendServerMessageInMain(String message){
        mainTextChannel.sendMessage("***" + message + "***").queue();

    }

    //descr: updates stock markey by editting. I don't entirely know how this works
    public static void updateExistingMessage(TextChannel channel, String msgPath, String msgFormat){//make version for stock market
        channel.retrieveMessageById(plugin.getConfig().getLong(msgPath)).queue((message) -> {
            // use the message here, its an async callback
            message.editMessage(msgFormat).queue();
            System.out.println("Updated message!");
        }, (failure) -> {
            // if the retrieve request failed this will be called (also async)
            if (failure instanceof ErrorResponseException) {
                ErrorResponseException ex = (ErrorResponseException) failure;
                if (ex.getErrorResponse() == ErrorResponse.UNKNOWN_MESSAGE) {
                    // this means the message doesn't exist
                    System.out.println("FAILED TO FIND MESSAGE MUST MANUALLY FIX");
                }
            }
            failure.printStackTrace();
        });
    }

    public static void updateStockMarketMessage(){
        updateExistingMessage(stockMarketTextChannel, STOCK_TEXT_CHANNEL_MSG_PATH, stockMarketMessageFormat());
    }

    public static String stockMarketMessageFormat(){
        Map<Material, Double> stockMarketChange = plugin.getStockMarket().comparePresentMarketToOld();
        Map<Material, Integer> stockMarket = plugin.getStockMarket().getItemStockMarket();
        StringBuilder stockMarketMessageString = new StringBuilder(1000);
        stockMarketMessageString.append("-------------Current Item Tracking (Updated Every 10 minutes)--------------\n");
        for(Material item : stockMarket.keySet()){
            if(plugin.getConfig().getString("emojis." + item.toString()) != null){
                String emoji = plugin.getConfig().getString("emojis." + item.toString());
                stockMarketMessageString.append(emoji);
            }
            else {
                stockMarketMessageString.append(item.toString());
            }
                stockMarketMessageString.append(" : ").append(stockMarket.get(item).toString())
                    .append(" ---------- ").append(stockMarketChange.get(item).intValue()).append("% change in last 10 minutes!")
                    .append("\n");

        }
        stockMarketMessageString.append("------------------------------------------------------------------------------------------");
        return stockMarketMessageString.toString();
    }

    public static void updateOnlinePlayersList(){
        updateExistingMessage(onlinePlayersChannel, ONLINE_PLAYERS_MSG_PATH, onlinePlayersListFormat());
    }

    public static String onlinePlayersListFormat(){
        StringBuilder playersOnlineMessageString = new StringBuilder(1000);
        int unregisteredPlayers = 0;
        playersOnlineMessageString.append("------ONLINE PLAYERS------\n");
        for(Player player : Bukkit.getOnlinePlayers()){
            if(PlayerDataHandler.isRegistered(player)){
                playersOnlineMessageString.append(nameWithoutTag(PlayerDataHandler.getTagfromPlayer(player)) + "\n");
            }
            else unregisteredPlayers++;
        }
        if(unregisteredPlayers != 0)
            playersOnlineMessageString.append("and " + unregisteredPlayers + " unregistered players...");

        return playersOnlineMessageString.toString();
    }

}

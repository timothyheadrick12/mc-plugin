package org.mcplugin;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockCookEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.*;

public class StockMarketListener implements Listener {

    private final long SAVE_FREQUENCY = 12000L;
    private final String STOCK_MARKET_PATH = "stockmarket.";
    private final List<Material> defaultTrackedItems = Arrays.asList(Material.IRON_BARS,
            Material.DIAMOND,
            Material.COAL,
            Material.GOLD_INGOT,
            Material.NETHERITE_SCRAP,
            Material.COBBLESTONE,
            Material.NETHER_STAR,
            Material.REDSTONE,
            Material.LAPIS_LAZULI,
            Material.EMERALD);

    private JitPlugin plugin;
    private Map<Material, Integer> itemStockMarket;
    private ArrayList<String> trackedItems;


    //descr: gets stock market from config file if it exists and sets the autosave to once every 10 mins
    //post: itemStockMarket made and autosave setup
    public StockMarketListener(JitPlugin instance){
        plugin = instance;
        itemStockMarket = new HashMap<>();
        getCreateStockMarket(itemStockMarket);
        BukkitScheduler scheduler = plugin.getServer().getScheduler();
        scheduler.scheduleSyncRepeatingTask(plugin, new Runnable() {
            @Override
            public void run() {
                updateAndSaveStockMarket();
            }
        }, SAVE_FREQUENCY, SAVE_FREQUENCY);
    }

    //descr: gets the stock market from the config file or creates one if it does not exist
    public void getCreateStockMarket(Map<Material, Integer> stockMarket){
        try{//this gets the map on startup if possible
            plugin.getConfig().getConfigurationSection(STOCK_MARKET_PATH).getKeys(false)
                    .forEach(material -> stockMarket.put(Material.getMaterial(material),
                            plugin.getConfig().getInt(STOCK_MARKET_PATH + material)));
            System.out.println("Successfully got Map: " + stockMarket);
        } catch (NullPointerException e){
            System.out.println("Creating Default items!");
            for(Material material : defaultTrackedItems){
                stockMarket.put(material, 0);
            }
            e.printStackTrace();
        }
    }

    //gets stockMarket
    public Map<Material, Integer> getItemStockMarket() {
        return itemStockMarket;
    }

    //saves the stock market after updating it on discord
    public void updateAndSaveStockMarket() {
        DiscordChatHandler.updateStockMarketMessage();
        BukkitScheduler scheduler = plugin.getServer().getScheduler();
        scheduler.scheduleSyncDelayedTask(plugin, new Runnable() {//task runs 10 seconds after
            @Override
            public void run() {
                saveStockMarket();
            }
        }, 20L);
    }

    //saves the stock market without updating it on discord
    public void saveStockMarket(){
        itemStockMarket.keySet().forEach(material -> plugin.getConfig().set(STOCK_MARKET_PATH + material.toString(), itemStockMarket.get(material)));
        plugin.saveConfig();
        System.out.println("Saving stock Market...");
    }

    //descr: adds item to tracked items. WARNING NOT GUARANTEED TO WORK!!!
    // Item might not track correctly with the given listeners
    //pre: item must be Material
    //post: item added to tracked items. returns true if successful and false otherwise
    public boolean addTrackedItem(Material item, Integer startingAmount){
        if(itemStockMarket.containsKey(item))
            return false;
        else{
            itemStockMarket.put(item, startingAmount);
            updateAndSaveStockMarket();
            return true;
        }
    }

    //descr: returns a map of doubles that is change since the stock market was last updated for each material.
    // can't handle negative percentages but that hopefully shouldn'y matter lmao
    public Map<Material, Double> comparePresentMarketToOld(){
        Map<Material, Double> changeInStockMarket = new HashMap<Material, Double>();
        Map<Material, Integer> oldStockMarket = new HashMap<Material, Integer>();
        getCreateStockMarket(oldStockMarket);
        for(Material item : itemStockMarket.keySet()){
            if(oldStockMarket.containsKey(item)){
                double percentChange = (itemStockMarket.get(item).doubleValue()/oldStockMarket.get(item).doubleValue()-1)*100;
                changeInStockMarket.put(item, percentChange);
            }
            else{//if the value is null in old it returns a stupid percent
                double percentChange = (itemStockMarket.get(item).doubleValue()/2-1)*100;
                changeInStockMarket.put(item, percentChange);
            }
        }
        return changeInStockMarket;
    }

    //descr:adds item to stock market everytime one is picked up
    //post: item added to stock market either in new or old value
    @EventHandler
    public void onPickupItem(EntityPickupItemEvent event){
        Material material = event.getItem().getItemStack().getType();
        if (itemStockMarket.containsKey(material)) {
            Integer amount = event.getItem().getItemStack().getAmount();
            Integer oldVal = itemStockMarket.get(material);
            Integer newVal = oldVal + amount;
            itemStockMarket.put(material, newVal);
        }
    }

    //descr: for cases of smelting like iron and such. This is different enough that methods to handle both would notbe worth it
    @EventHandler
    public void onSmelt(BlockCookEvent event){
        Material material = event.getResult().getType();
        if(itemStockMarket.containsKey(material)) {
            Integer amount = event.getResult().getAmount();
            Integer oldVal = itemStockMarket.get(material);
            Integer newVal = oldVal + amount;
            itemStockMarket.put(material, newVal);
        }
    }

    @EventHandler
    public void onBurn(FurnaceBurnEvent event){
        Material material = event.getFuel().getType();
        if(itemStockMarket.containsKey(material)) {
            Integer oldVal = itemStockMarket.get(material);
            Integer newVal = oldVal - 1;
            itemStockMarket.put(material, newVal);
        }
    }

    //handles item crafting for deleting and gaining items
    @EventHandler
    public void onCraft(CraftItemEvent event){
        CraftingInventory items = event.getInventory();
        ItemStack craftedItem = event.getRecipe().getResult();
        int amount = craftedItem.getAmount();
        if(event.getClick().isShiftClick())
        {
            int lowerAmount = craftedItem.getMaxStackSize() + 1000; //Set lower at recipe result max stack size + 1000 (or just highter max stacksize of reciped item)
            for(ItemStack actualItem : items.getContents()) //For each item in crafting inventory
            {
                if(!actualItem.getType().isAir() && lowerAmount > actualItem.getAmount() && !actualItem.getType().equals(craftedItem.getType())) //if slot is not air && lowerAmount is highter than this slot amount && it's not the recipe amount
                    lowerAmount = actualItem.getAmount(); //Set new lower amount
            }
            //Calculate the final amount : lowerAmount * craftedItem.getAmount
            amount = lowerAmount * craftedItem.getAmount();
            System.out.println(amount);
        }
        try {
            Material material = craftedItem.getType();
            Material craftMaterial;
            System.out.println(items);
            if (itemStockMarket.containsKey(material)) {
                Integer oldVal = itemStockMarket.get(material);
                Integer newVal = oldVal + amount;
                itemStockMarket.put(material, newVal);
            } else {
                for (ItemStack item : items.getContents()) {
                    craftMaterial = item.getType();
                    if (itemStockMarket.containsKey(craftMaterial)) {
                        Integer oldVal = itemStockMarket.get(craftMaterial);
                        Integer newVal = oldVal - amount;
                        itemStockMarket.put(craftMaterial, newVal);
                    }
                }
            }
        } catch (NullPointerException e){
            System.out.println("Something went wrong while tracking crafting!");
        }
    }

    @EventHandler
    public void dropItem(PlayerDropItemEvent event){
        Material material = event.getItemDrop().getItemStack().getType();
        if(itemStockMarket.containsKey(material)){
            Integer amount = event.getItemDrop().getItemStack().getAmount();
            Integer oldVal = itemStockMarket.get(material);
            Integer newVal = oldVal - amount;
            itemStockMarket.put(material, newVal);
        }
    }

    @EventHandler
    public void playerDeathDropItem(PlayerDeathEvent event){
        for(ItemStack item : event.getDrops()){
            Material material = item.getType();
            if (itemStockMarket.containsKey(material)) {
                Integer amount = item.getAmount();
                Integer oldVal = itemStockMarket.get(material);
                Integer newVal = oldVal - amount;
                itemStockMarket.put(material, newVal);
            }
        }
    }

    /*
    Deprecated because cannot work properly without tons of overhead on the server
    @EventHandler
    public void playerMoveItem(InventoryClickEvent event){
        Material material = event.getCurrentItem().getType();
        if(itemStockMarket.containsKey(material)){
            Inventory top = event.getView().getTopInventory();//probably a chest or crafting table or the like
            Inventory bottom = event.getView().getBottomInventory();//player inventory most likely
            System.out.println("Got size: " + top.getSize());
            if(event.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY){
                if(top.getSize() < CHEST_MIN_SIZE && top.getType() != InventoryType.HOPPER){
                    InventoryType clickedInventory = event.getClickedInventory().getType();
                    System.out.println(top.getType() + " : " + clickedInventory);
                    if(clickedInventory == top.getType()){//whenever the player takes something out of the potential storage location
                        Integer amount = event.getCurrentItem().getAmount();
                        Integer oldVal = itemStockMarket.get(material);
                        Integer newVal = oldVal + amount;
                        itemStockMarket.put(material, newVal);
                        System.out.println(material + " : " + itemStockMarket.get(material).toString());
                    }
                    else if(clickedInventory == bottom.getType()){
                        Integer amount = event.getCurrentItem().getAmount();
                        Integer oldVal = itemStockMarket.get(material);
                        Integer newVal = oldVal - amount;
                        itemStockMarket.put(material, newVal);
                        System.out.println(material + " : " + itemStockMarket.get(material).toString());
                    }
                }
            }
        }
    }
     */

}

package org.mcplugin;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class CommandAddTrackedItem implements CommandExecutor {

    private final String NOT_HOLDING_ITEM = "You must be holding the item you want to track!";
    private final String ITEM_ALREADY_REGISTERED_MESSAGE = "That item is already being tracked!";
    private final String REGISTERED_ITEM_MESSAGE = "Successfully registered item: ";

    private JitPlugin plugin;
    private StockMarketListener stockMarket;

    CommandAddTrackedItem(JitPlugin instance){
        plugin = instance;
        stockMarket = instance.getStockMarket();
    }

    @Override//called when command is run
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(sender instanceof Player) {
            Player player = (Player) sender;
            Material heldItem = player.getInventory().getItemInMainHand().getType();
            if (heldItem == Material.AIR) {
                player.sendMessage(NOT_HOLDING_ITEM);
            }
            else if(stockMarket.getItemStockMarket().containsKey(heldItem)){
                player.sendMessage(ITEM_ALREADY_REGISTERED_MESSAGE);
            }
            else{
                stockMarket.addTrackedItem(heldItem,
                        player.getInventory().getItemInMainHand().getAmount());
                player.sendMessage(REGISTERED_ITEM_MESSAGE + heldItem);
            }
            return true;
        }
        else {
            return false;
        }
    }
}

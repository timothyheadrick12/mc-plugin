package org.mcplugin;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

import static org.bukkit.Bukkit.getOfflinePlayer;

public class PlayerDataHandler {

    private static final String USER_KEY_PATH = "users.";
    private static final String DISCORD_KEY_PATH = ".discord";
    private static final String USER_UUID_DISCORD_LINK_PATH = "users.linktodiscordmap.";
    private static BidiMap<UUID, String> mcDiscordLink;
    private static HashMap<UUID, Boolean> mcDiscordRegistered;

    private static FileConfiguration playerData;
    private static JitPlugin plugin;

    public static void initializePlayerDataHandler(JitPlugin passedPlugin, FileConfiguration passedPlayerData){
        plugin = passedPlugin;
        playerData = passedPlayerData;
        mcDiscordLink = new DualHashBidiMap<>();
        try{//this gets the map on startup if possible
            playerData.getConfigurationSection(USER_KEY_PATH).getKeys(false)//for each uuid it is changed to a UUID and the tag is gotten
                    .forEach(uuid -> mcDiscordLink.put(UUID.fromString(uuid), playerData.getString(USER_KEY_PATH + uuid + DISCORD_KEY_PATH)));
            System.out.println("Successfully got bidimap: " + mcDiscordLink);
        } catch (NullPointerException e){
            System.out.println("No users have been registered to discord!");
        }

    }

    //descr: adds the new entry to the config and the map and saves it to playerData config
    //pre: must be valid discord tag and Player
    //post: key value pairs saved to playerData.yml and map updated
    public static void linkUUIDandDiscord(Player player, String tag){

        UUID playerUUID = player.getUniqueId();
        mcDiscordLink.put(player.getUniqueId(), tag);
        playerData.set(USER_KEY_PATH + playerUUID.toString() + DISCORD_KEY_PATH, tag);
        plugin.savePlayerData();
    }

    public static void saveMcDiscordLink()
    {
        mcDiscordLink.keySet().forEach(uuid -> playerData.set(USER_KEY_PATH + uuid.toString() + DISCORD_KEY_PATH, mcDiscordLink.get(uuid)));
    }

    public static OfflinePlayer getOfflinePlayerfromDiscord(String tag){
        return getOfflinePlayer(mcDiscordLink.getKey(tag));
    }

    public static String getTagfromPlayer(Player player){
        return mcDiscordLink.get(player.getUniqueId());
    }

    public static boolean isRegistered(Player player){
        return mcDiscordLink.containsKey(player.getUniqueId());
    }

    public static boolean isRegistered(String tag){
        return mcDiscordLink.containsValue(tag);
    }




}

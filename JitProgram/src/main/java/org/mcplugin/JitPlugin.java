package org.mcplugin;
import javax.security.auth.login.LoginException;

import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.ChunkingFilter;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

public class JitPlugin extends JavaPlugin {
    private File playerDataFile;
    private FileConfiguration playerData;
    private FileConfiguration config = getConfig();
    private Server server = Bukkit.getServer();
    private JDA bot;
    private StockMarketListener stockMarket;
    /*config file can be used to create user set things and save choices
     * to access it from other java files use jitPlugin.getconfig(). It is also possible
     * to create multiple config files! Added for test to linux
     */

    // Fired when plugin is first enabled
    @Override
    public void onEnable() {
        this.saveDefaultConfig();//saves the config file on startup. will not overwrite the config if it exists

        /*------------------------------------------EVENTS AND COMMANDS-----------------------------------------------------------*/
        stockMarket = new StockMarketListener(this);
        getServer().getPluginManager().registerEvents(new McChatListener(this), this);//need to do this for each new class
        getServer().getPluginManager().registerEvents(stockMarket, this);

        this.getCommand("dregister").setExecutor(new CommandRegisterDiscord());//need this for each command
        this.getCommand("trackItem").setExecutor(new CommandAddTrackedItem(this));
        this.getCommand("tracker").setExecutor(new CommandGetStockMarket(this));
        /*------------------------------------------CONNECT TO PLAYERDATA-----------------------------------------------------------*/
        createPlayerData();
        PlayerDataHandler.initializePlayerDataHandler(this, playerData);

        /*------------------------------------------BOT CONFIGURATION------------------------------------------------------------*/
        setupBot();
        DiscordChatHandler.initializeHandler(this);


    }

    // Fired when plugin is disabled
    @Override
    public void onDisable() {
        stockMarket.saveStockMarket();
    }

    //getter for bot
    @NotNull
    public JDA getBot() {
        return bot;
    }

    //getter for playerData
    public FileConfiguration getPlayerData() {
        return this.playerData;
    }
    //getter for stockMarket
    public StockMarketListener getStockMarket() {
        return stockMarket;
    }

    //saves the playerData file
    public void savePlayerData(){
        try {
            playerData.save(playerDataFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //descr: this sets up the Bot
    private void setupBot() {
        JDABuilder jdaBuilder = JDABuilder.createDefault(getConfig().getString("discord.bot"));//sets bot to key in config
        jdaBuilder.setActivity(Activity.playing("Running Minecraft Server"))//says what bot is playing in discord
            .enableIntents(GatewayIntent.GUILD_MEMBERS)
            .setMemberCachePolicy(MemberCachePolicy.ALL)
            .setChunkingFilter(ChunkingFilter.ALL)
                .addEventListeners(new DiscordChatListener());//need this for each listener added
            //this code is fucked it basically makes the bot cache every member in existence
            //that's like super bad, but it is only going to be on one server so who cares.
        try {
            bot = jdaBuilder.build();//this requires an exception catch shouldn't really happen though
            bot.awaitReady();//waits until bot is logged in
            //DiscordChatHandler.initializeHandler(this);//puts bot into DiscordChatHandler
        } catch (LoginException | InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Bot booted!");
    }

    //descr: this handles everything related to the customConfig file playerData
    private void createPlayerData() {
        playerDataFile = new File(getDataFolder(), "playerData.yml");//create new file in dataFolder (found in Plugin folder)
        if (!playerDataFile.exists()) {//if it doesn't exist create it
            playerDataFile.getParentFile().mkdirs();
            saveResource("playerData.yml", false);//save without replacing when it doesn't exist
        }

        playerData = new YamlConfiguration();//allows it to be editted like normal config
        //Needs to be saved differently with .save("playerData.yml");
        try {
            playerData.load(playerDataFile);//turns the playerData file into proper yml
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }
}

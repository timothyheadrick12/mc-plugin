package org.mcplugin;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;



public class CommandRegisterDiscord implements CommandExecutor {

    private final String COMMAND_SUCCESS_MSG = "Successfully linked Minecraft to Discord!";
    private final String COMMAND_FAILURE_MSG = "That was not a valid Discord username. Please try again.";

    @Override//called when command is run
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(args.length != 0) {//makes sure there is at least some argument
            String discordTag = args[0];
            if (sender instanceof Player) {//checks that a player sent it
                Player player = (Player) sender;
                if (DiscordChatHandler.validTag(discordTag)) {//validates the tag then sets it int playerData
                    PlayerDataHandler.linkUUIDandDiscord(player, discordTag);
                    DiscordChatHandler.register(discordTag);
                    sender.sendMessage(COMMAND_SUCCESS_MSG);
                    return true;
                } else {
                    sender.sendMessage(COMMAND_FAILURE_MSG);
                    return false;
                }
            }
        }
        return false;
    }
}

# MC Plugin

Folder for all Minecraft server plugin related code

Some information for building it:
    
    You must use Maven to build the project
    It will not run otherwise because shading is 
    required for all classes to be packaged together.

Information for updating git and collaboration:
    
    NEVER commit to master. Always create a new branch when working on something
    and commit and push that branch regularly so that everything is up to date.
    I.e. commit everytime it builds without errors

    Whenever a new feature is done submit a merge request, and I will go in and make
    sure everything works. Only then will I push to master. Everytime a new branch is merged to master,
    pull from master so that your code is up to date on whatever branch you are working on.
    It is possible to merge master into another branch if needed. I.e you are in the middle
    of developing a feature when master is updated. You want to merge master into your branch
    as soon as possible to make sure that something new about master does not break whatever you
    are currently working on.
